Welcome to `omni_platform`, a package that simplifies the cross-platform development process by providing a virtual platform than can be changed at runtime, virtual platform instruments and physical platform instruments.

`omni_platform` integrates the packages [`virtual_platform`](https://pub.dev/packages/virtual_platform) and [`physical_platform`](https://pub.dev/packages/physical_platform), without adding any extra functionality. This way you only need to declare one package in your `pubspec.yaml`.

## Getting started

You need to add `omni_platform` to your dependencies.

```yaml
dependencies:
  omni_platform: ^latest # replace latest with version number
```

Next, you have to import `package:omni_platform/omni_platform.dart`.

## Usage


- Virtual platform docs

  Please consult the [`virtual_platform` package documentation](https://pub.dev/packages/virtual_platform).

- Physical platform docs

  Please consult the [`physical_platform` package documentation](https://pub.dev/packages/physical_platform).

## Platforms and platform groups

| *Platform* | appleSystems | mobileSystems | desktopSystems |
| ---------- |:------------:|:-------------:|:--------------:|
| android    |              | ✓             |                |
| ios        | ✓            | ✓             |                |
| linux      |              |               | ✓              |
| macos      | ✓            |               | ✓              |
| windows    |              |               | ✓              |
| web        |              |               |                |
| fuchsia    |              |               |                |

Priority order: from left to right.