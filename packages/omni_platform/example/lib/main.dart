import 'package:flutter/material.dart';
import 'package:omni_platform/omni_platform.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  VirtualPlatform.notifier = VirtualPlatformNotifier(VirtualPlatforms.android);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Virtual Platform Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            VirtualPlatformDispatcher(
              linux: (_, __) => const Text("Linux"),
              android: (_, __) => const Text("android"),
              other: (_, __) => const Text("other"),
            ),
          ],
        ),
      ),
    );
  }
}
