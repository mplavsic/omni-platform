## 0.3.1

Update dependencies.

## 0.3.0

* Integrates packages `physical_platform` and `virtual_platform`.

Versioning starts at `0.3.0` to match the first version of `virtual_platform` that takes into account package splitting (into `abstract_platform`, `physical_platform`, `virtual_platform`, `omni_platform`).