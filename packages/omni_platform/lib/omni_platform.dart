/// A package that integrates the declarative and pragmatic instruments
/// provided by the packages virtual_platform and physical_platform.
library omni_platform;

export 'package:virtual_platform/virtual_platform.dart';
export 'package:physical_platform/physical_platform.dart';
