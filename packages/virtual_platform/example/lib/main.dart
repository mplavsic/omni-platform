import 'package:flutter/material.dart';
import 'package:virtual_platform/virtual_platform.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  VirtualPlatform.notifier = VirtualPlatformNotifier(VirtualPlatforms.android);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Virtual Platform Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            VirtualPlatformDispatcher(
              other: (_, __) => TextButton(
                child: const Text('set to ios'),
                onPressed: () => VirtualPlatform.notifier.chosenPlatform =
                    VirtualPlatforms.ios,
              ),
              ios: (_, __) => TextButton(
                child: const Text('set to android'),
                onPressed: () => VirtualPlatform.notifier.chosenPlatform =
                    VirtualPlatforms.android,
              ),
            ),
            VirtualPlatformDispatcher(
              linux: (_, __) => const Text("linux"),
              android: (_, __) => const Text("android"),
              macos: (context, child) => const Text("macos"),
              other: (_, __) => const Text("other"),
            ),
          ],
        ),
      ),
    );
  }
}
