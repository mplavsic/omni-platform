import 'package:flutter/widgets.dart';

import 'virtual_platform.dart';

class VirtualPlatformNotifier extends ValueNotifier<VirtualPlatform> {
  VirtualPlatformNotifier(VirtualPlatform init) : super(init);

  set chosenPlatform(VirtualPlatform platform) {
    value = platform;
    notifyListeners();
  }
}
