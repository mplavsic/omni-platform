import 'package:abstract_platform/abstract_platform.dart';
import 'package:flutter/material.dart' show Material;
import 'package:flutter/widgets.dart';

import 'virtual_platform.dart';

/// Builds a [Widget] based on the virtual platform.
class VirtualPlatformDispatcher extends AbstractPlatformDispatcher {
  /// Selects the widget builder to invoke that matches the virtual platform.
  /// If the virtual platform changes, a rebuild will occur.
  /// Use this builder for ***platform-specific UI***.
  ///
  /// **[other] chain**
  ///
  /// [other] will be selected as default chain in case all
  /// other chains do not match.
  ///
  /// However, note that specifying [other] does not automatically bring safety,
  /// i.e.,
  /// a crash might occur even if the [other] chain is specified. For example,
  /// let us assume the virtual platform is [iosVirtualPlatform] and the widget
  /// tree consists of Cupertino widgets.
  /// If the matched chain is [other] and this chain builds a widget using the
  /// Material design, this widget should be wrapped in a [Material] before
  /// getting added to the "Cupertino" widget tree. If not, it might result
  /// in a crash.
  ///
  /// **Platform groups**
  ///
  /// [apple], [desktop] and [mobile] are platform groups.
  ///
  /// - [mobile] will be selected if, at runtime, the platform is one of:
  ///   - [android]
  ///   - [ios]
  /// - [desktop] will be selected if, at runtime, the platform is one of:
  ///   - [linux]
  ///   - [macos]
  ///   - [windows]
  /// - [apple] will be selected if, at runtime, the platform is one of:
  ///   - [ios]
  ///   - [macos]
  ///
  /// Platforms have precedence over platform groups. [apple] has precedence
  /// over both [desktop] and [mobile].
  const VirtualPlatformDispatcher({
    // super-class params:
    super.key,
    required super.other,
    super.android,
    super.fuchsia,
    super.ios,
    super.linux,
    super.macos,
    super.windows,
    super.web,
    super.appleSystems,
    super.desktopSystems,
    super.mobileSystems,
    super.child,
  });

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<VirtualPlatform>(
      child: child,
      valueListenable: VirtualPlatform.notifier,
      builder: (context, abstractPlatform, child) {
        return buildCore(context, abstractPlatform);
      },
    );
  }
}
