import 'package:abstract_platform/abstract_platform.dart';

import 'virtual_platform_notifier.dart';

class VirtualPlatform extends AbstractPlatform {
  const VirtualPlatform._(super.name);

  // The 3 overrides below are needed only by this class and not by
  // [PhysicalPlatform], therefore they don't have to be specified in
  // [AbstractPlatform].

  /// Useful when saving the used [VirtualPlatform] to persistent storage.
  @override
  String toString() => name;

  /// When changing state it is important that the name of the selected platform differs from the old state's one.
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AbstractPlatform &&
          runtimeType == other.runtimeType &&
          name == other.name;

  @override
  int get hashCode => name.hashCode;

  /// This notifier must be instantiated before it is listened to.
  static late final VirtualPlatformNotifier notifier;

  /// Useful when saving the newly selected [VirtualPlatform] to persistent
  /// storage.
  ///
  /// NB: [notifier] needs to be initialized first.
  static VirtualPlatform get current => notifier.value;

  /// The virtual platform representation of the actual platform.
  static VirtualPlatform get physical => fromString(physicalPlatformAsString);

  /// Useful when loading the last used [VirtualPlatform] from persistent
  /// storage.
  ///
  /// It will default to the corresponding physical platform if the [platformName]
  /// is invalid.
  static VirtualPlatform fromString(String platformName) {
    switch (platformName) {
      case "android":
        return VirtualPlatforms.android;
      case "ios":
        return VirtualPlatforms.ios;
      case "linux":
        return VirtualPlatforms.linux;
      case "macos":
        return VirtualPlatforms.macos;
      case "windows":
        return VirtualPlatforms.windows;
      case "web":
        return VirtualPlatforms.fuchsia;
      case "fuchsia":
        return VirtualPlatforms.fuchsia;
      default:
        return fromString(
            physicalPlatformAsString); // in case the value loaded from shared preferences is invalid
    }
  }
}

/// All possible virtual platforms.
class VirtualPlatforms {
  const VirtualPlatforms._();
  static const android = VirtualPlatform._('android');
  static const ios = VirtualPlatform._('ios');
  static const linux = VirtualPlatform._('linux');
  static const macos = VirtualPlatform._('macos');
  static const windows = VirtualPlatform._('windows');
  static const web = VirtualPlatform._('web');
  static const fuchsia = VirtualPlatform._('fuchsia');
}
