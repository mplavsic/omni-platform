/// A package that simplifies the cross-platform development process by
/// providing a virtual platform than can be changed at runtime.
library virtual_platform;

export 'src/match_virtual_platform.dart';
export 'src/virtual_platform_dispatcher.dart';
export 'src/virtual_platform_notifier.dart';
export 'src/virtual_platform.dart';
