## 0.3.1

Improve documentation

## 0.3.0

This release only contains the instrument present in the library `virtual_platform`. The package was split into multiple packages `abstract_platform`, `physical_platform`, `virtual_platform`, `omni_platform`.

The subplatform concept was completely removed.

Therefore, this release features:
* VirtualPlatform
* VirtualPlatformDispatcher
* matchVirtualPlatform

## 0.2.0
This release makes it easier to distinguish between platform groups only targeting platforms (by appending "Systems") and subplatform groups mostly taking into account the available screen size.
- Change platform groups to "appleSystems", "mobileSystems" and "desktopSystems".
- Change "apple" subplatform group to "appleSystems". Keep "mobile" and "desktop" subplatforms.

## 0.1.1
Pass static analysis and add docstring.

## 0.1.0
The package was split into 3 libraries. The documentation was rewritten.

## 0.0.1        
First release. It comes with:
* `matchPhysicalPlatform`
* `matchVirtualPlatform`
* `VirtualPlatformBuilder`
* `ResponsiveBuilder`
* `VirtualPlatformNotifier`
* `VirtualPlatform`