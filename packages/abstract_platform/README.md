Welcome to `abstract_platform`, a package that provides abstract classes needed by [`virtual_platform`](https://pub.dev/packages/virtual_platform), by [`physical_platform`](https://pub.dev/packages/physical_platform), and — indirectly — by [`omni_platform`](https://pub.dev/packages/omni_platform).

Do not add this package to your `pubspec.yaml`; instead, add one of the other three ones.