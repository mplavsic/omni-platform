import 'dart:io';

import 'package:flutter/foundation.dart';

abstract class AbstractPlatform {
  // Name of the platform (physical or virtual), such as "linux", "macos", ...
  final String name;

  const AbstractPlatform(this.name);
}

/// This getter is needed for for both classes:
/// - [PhysicalPlatform] (for obvious reasons)
/// - [VirtualPlatform] (for fallbacks based on the physical platform)
///
/// NB: This getter is not a static getter of [AbstractPlatform] because it
/// should be used only internally in [PhysicalPlatform]
/// and [VirtualPlatform] (thus, without getting exposed).
String get physicalPlatformAsString =>
    kIsWeb ? 'web' : Platform.operatingSystem;
