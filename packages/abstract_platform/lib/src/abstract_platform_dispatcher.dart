import 'package:flutter/widgets.dart';

import 'abstract_platform.dart';
import 'utils/chain.dart';

abstract class AbstractPlatformDispatcher extends StatelessWidget {
  final Widget? child;

  final TransitionBuilder other;
  final TransitionBuilder? android;
  final TransitionBuilder? fuchsia;
  final TransitionBuilder? ios;
  final TransitionBuilder? linux;
  final TransitionBuilder? macos;
  final TransitionBuilder? windows;
  final TransitionBuilder? web;
  final TransitionBuilder? appleSystems;
  final TransitionBuilder? desktopSystems;
  final TransitionBuilder? mobileSystems;

  const AbstractPlatformDispatcher({
    super.key,
    required this.other,
    this.android,
    this.fuchsia,
    this.ios,
    this.linux,
    this.macos,
    this.windows,
    this.web,
    this.appleSystems,
    this.desktopSystems,
    this.mobileSystems,
    this.child,
  });

  /// The physical platform dispatcher builder directly calls this code.
  /// On the other hand, the virtual platform dispatcher's one calls it inside
  /// the [ValueListenableBuilder]'s builder body.
  Widget buildCore(BuildContext context, AbstractPlatform abstractPlatform) {
    final TransitionBuilder? platformChain;
    switch (abstractPlatform.name) {
      case 'android':
        platformChain = tbChain([android, mobileSystems]);
        break;
      case 'ios':
        platformChain = tbChain([ios, appleSystems, mobileSystems]);
        break;
      case 'linux':
        platformChain = tbChain([linux, desktopSystems]);
        break;
      case 'macos':
        platformChain = tbChain([macos, appleSystems, desktopSystems]);
        break;
      case 'windows':
        platformChain = tbChain([windows, desktopSystems]);
        break;
      case 'web':
        platformChain = web;
        break;
      case 'fuchsia':
        platformChain = fuchsia;
        break;
      default:
        platformChain = null;
    }
    return tbChain([platformChain, other])!.call(context, child);
  }
}
