import 'abstract_platform.dart';
import 'utils/chain.dart';

/// Immediately invokes a function matching the ***abstract*** platform.
///
/// **[other] chain**
///
/// [other] will be selected as default chain at runtime
/// in case the platform (or a platform group containing it) is not
/// specified.
T matchAbstractPlatform<T>({
  required AbstractPlatform abstractPlatform,
  required T Function() other,
  T Function()? android,
  T Function()? fuchsia,
  T Function()? ios,
  T Function()? linux,
  T Function()? macos,
  T Function()? windows,
  T Function()? web,
  T Function()? appleSystems,
  T Function()? desktopSystems,
  T Function()? mobileSystems,
}) {
  final T Function()? platformChain;
  switch (abstractPlatform.name) {
    case 'android':
      platformChain = chain([android, mobileSystems]);
      break;
    case 'ios':
      platformChain = chain([ios, appleSystems, mobileSystems]);
      break;
    case 'linux':
      platformChain = chain([linux, desktopSystems]);
      break;
    case 'macos':
      platformChain = chain([macos, appleSystems, desktopSystems]);
      break;
    case 'windows':
      platformChain = chain([windows, desktopSystems]);
      break;
    case 'web':
      platformChain = web;
      break;
    case 'fuchsia':
      platformChain = fuchsia;
      break;
    default:
      platformChain = null;
  }
  return chain([platformChain, other])!.call();
}
