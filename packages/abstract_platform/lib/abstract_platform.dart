/// A package that simplifies the cross-platform development process by
/// providing a virtual platform than can be changed at runtime.
library abstract_platform;

export 'src/abstract_platform.dart';
export 'src/abstract_platform_dispatcher.dart';
export 'src/match_abstract_platform.dart';
export 'src/utils/chain.dart';
