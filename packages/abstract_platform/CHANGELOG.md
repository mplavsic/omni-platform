## 0.3.0

This release features:
* AbstractPlatform
* AbstractPlatformDispatcher
* matchAbstractPlatform
* chain util (not exported to lib)

Versioning starts at `0.3.0` to match the first version of `virtual_platform` that takes into account package splitting (into `abstract_platform`, `physical_platform`, `virtual_platform`, `omni_platform`).