## 0.3.1

Improve documentation

## 0.3.0

This release features:
* PhysicalPlatform (not exported to lib)
* PhysicalPlatformDispatcher
* matchPhysicalPlatform

Versioning starts at `0.3.0` to match the first version of `virtual_platform` that takes into account package splitting (`abstract_platform`, `physical_platform`, `virtual_platform`, `omni_platform`).