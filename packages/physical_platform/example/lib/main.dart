import 'package:flutter/material.dart';
import 'package:physical_platform/physical_platform.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(title: const Text('example')),
        body: Builder(builder: (context) {
          return PhysicalPlatformDispatcher(
            mobileSystems: (_, __) =>
                const WebView(initialUrl: "https://wikipedia.org"),
            other: (_, __) => Center(
              child: Text(
                  'Platform doesn\'t support the official webview\n${context.size}'),
            ),
          );
        }),
      ),
    );
  }

  Widget a() {
    return ElevatedButton(
      onPressed: () {
        // do smth with context
      },
      child: const Text('mobile'),
    );
  }
}
