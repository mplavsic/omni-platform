import 'package:abstract_platform/abstract_platform.dart';
import 'package:flutter/widgets.dart';
import 'package:physical_platform/src/physical_platform.dart';

/// Builds a [Widget] based on the physical platform.
class PhysicalPlatformDispatcher extends AbstractPlatformDispatcher {
  /// Selects the widget builder to invoke that matches the physical platform.
  /// Use this dispatcher for ***platform-specific Widgets***.
  ///
  /// Use-case example:
  /// - iOS devices need the official webview (flutter_webview)
  /// - Android devices might need a different one (flutter_inappwebview)
  /// (e.g. because the official one does not allow to upload files).
  /// - Other platforms don't use a webview at all.
  ///
  /// **[other] chain**
  ///
  /// [other] will be selected as default chain in case all
  /// other chains do not match.
  ///
  /// **Platform groups**
  ///
  /// [appleSystems], [desktopSystems] and [mobileSystems] are platform groups.
  ///
  /// - [mobileSystems] will be selected if, at runtime, the platform is one of:
  ///   - [android]
  ///   - [ios]
  /// - [desktopSystems] will be selected if, at runtime, the platform is one of:
  ///   - [linux]
  ///   - [macos]
  ///   - [windows]
  /// - [appleSystems] will be selected if, at runtime, the platform is one of:
  ///   - [ios]
  ///   - [macos]
  ///
  /// Platforms have precedence over platform groups. [appleSystems] has precedence
  /// over both [desktopSystems] and [mobileSystems].
  const PhysicalPlatformDispatcher({
    // super-class params:
    super.key,
    required super.other,
    super.android,
    super.fuchsia,
    super.ios,
    super.linux,
    super.macos,
    super.windows,
    super.web,
    super.appleSystems,
    super.desktopSystems,
    super.mobileSystems,
    super.child,
  });

  @override
  Widget build(BuildContext context) =>
      buildCore(context, PhysicalPlatform.thisDevice);
}
