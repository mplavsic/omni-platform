import 'package:abstract_platform/abstract_platform.dart';

class PhysicalPlatform extends AbstractPlatform {
  const PhysicalPlatform._(super.name);

  static final thisDevice = PhysicalPlatform._(physicalPlatformAsString);
}
