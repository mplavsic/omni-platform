/// A Flutter library package that simplifies the cross-platform development
/// process by providing declarative and pragmatic instruments.
library physical_platform;

export 'src/physical_platform_dispatcher.dart';
export 'src/match_physical_platform.dart';
