Welcome to the [`virtual_platform`](https://gitlab.com/mplavsic/omni-platform/-/tree/main/packages/virtual_platform), [`physical_platform`](https://gitlab.com/mplavsic/omni-platform/-/tree/main/packages/physical_platform) and [`omni_platform`](https://gitlab.com/mplavsic/omni-platform/-/tree/main/packages/omni_platform) repository.

## Descriptions

### `virtual_platform`
A Flutter package that simplifies the cross-platform development process by providing a virtual platform than can be changed at runtime.

### `physical_platform`
A Flutter package that simplifies the cross-platform development process by providing declarative and pragmatic instruments.

### `omni_platform`
A Flutter package that integrates the packages `virtual_platform` and `physical_platform`, without adding any extra functionality. This way you only need to declare one package in your `pubspec.yaml`.